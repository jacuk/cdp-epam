CREATE TABLE `todo_db` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `dueDate` varchar(100) NOT NULL,
  `createdDate` varchar(100) NOT NULL,
  `updatedDate` varchar(100) DEFAULT NULL,
  `priority` enum('CRITICAL','RUSH','NORMAL','LOW') DEFAULT NULL,
  KEY `id` (`id`)
)