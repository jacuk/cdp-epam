package com.epam.production.cdp.yatsenko.dao;

import java.util.List;

import com.epam.production.cdp.yatsenko.entity.ToDoItem;

public interface IToDoDAO {

	public ToDoItem findById(long id);

	public List<ToDoItem> findAll();

	public ToDoItem create(ToDoItem item);

	public ToDoItem update(ToDoItem item);

	public boolean remove(long id);

	public List<ToDoItem> findByPriority(String priority);

	public List<ToDoItem> findEndingOn(String endingDate);

}
