package com.epam.production.cdp.yatsenko.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TransactionRequiredException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.epam.production.cdp.yatsenko.entity.ToDoItem;

public class JpaToDoDAO implements IToDoDAO {

	private static final String PERSISTENCE_UNIT_NAME = "TODO";
	private static final String DUE_DATE = "dueDate";
	private static final String PRIORITY = "priority";

	private final EntityManager em = Persistence.createEntityManagerFactory(
			PERSISTENCE_UNIT_NAME).createEntityManager();

	public ToDoItem findById(long id) {
		return em.find(ToDoItem.class, id);
	}

	public List<ToDoItem> findAll() {
		TypedQuery<ToDoItem> todoList = em.createNamedQuery("ToDoItem.getAll",
				ToDoItem.class);
		return todoList.getResultList();
	}

	public ToDoItem create(ToDoItem item) {
		em.getTransaction().begin();
		ToDoItem todoItem = em.merge(item);
		em.getTransaction().commit();
		return todoItem;
	}

	public ToDoItem update(ToDoItem item) {
		em.getTransaction().begin();
		ToDoItem todoItem = em.merge(item);
		em.getTransaction().commit();
		return todoItem;
	}

	public boolean remove(long id) {
		em.getTransaction().begin();
		try {
			em.remove(findById(id));
		} catch (IllegalArgumentException | TransactionRequiredException e) {
			System.err.println(e.getMessage());
			return false;
		}
		em.getTransaction().commit();
		return true;
	}

	public List<ToDoItem> findByPriority(String priority) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ToDoItem> cq = cb.createQuery(ToDoItem.class);
		Root<ToDoItem> e = cq.from(ToDoItem.class);
		cq.select(e).where(cb.equal(e.get(PRIORITY), priority));
		Query query = em.createQuery(cq);
		return query.getResultList();
	}

	public List<ToDoItem> findEndingOn(String endingDate) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ToDoItem> cq = cb.createQuery(ToDoItem.class);
		Root<ToDoItem> e = cq.from(ToDoItem.class);
		cq.select(e).where(cb.equal(e.get(DUE_DATE), endingDate));
		Query query = em.createQuery(cq);
		return query.getResultList();
	}
}
