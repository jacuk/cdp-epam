package com.epam.production.cdp.yatsenko.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = ToDoItem.TODO_DB)
@NamedQuery(name = ToDoItem.TO_DO_ITEM_GET_ALL, query = ToDoItem.SELECT_TASK_FROM_TO_DO_ITEM)
public class ToDoItem implements Serializable {
	
	static final String TODO_DB = "todo_db";
	static final String TO_DO_ITEM_GET_ALL = "ToDoItem.getAll";
	static final String SELECT_TASK_FROM_TO_DO_ITEM = "SELECT task from ToDoItem task";

	private static final long serialVersionUID = 6122768259639382586L;
	private static final String TODO_ID = "id";
	private static final String TODO_TITLE = "title";
	private static final String TODO_DESCRIPTION = "description";
	private static final String TODO_DUE_DATE = "dueDate";
	private static final String TODO_CREATED_DATE = "createdDate";
	private static final String TODO_UPDATED_DATE = "updatedDate";
	private static final String TODO_PRIORITY = "priority";

	private long id;
	private String title;
	private String description;
	private String dueDate;
	private String createdDate;
	private String updatedDate;
	private String priority;

	public ToDoItem() {
	}

	public ToDoItem(int id, String title, String dueDate, String createdDate) {
		this.id = id;
		this.title = title;
		this.dueDate = dueDate;
		this.createdDate = createdDate;
	}

	public ToDoItem(int id, String title, String description, String dueDate,
			String createdDate, String updatedDate, String priority) {
		this.id = id;
		this.title = title;
		this.description = description;
		this.dueDate = dueDate;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
		this.priority = priority;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = TODO_ID, nullable = false)
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = TODO_TITLE, nullable = false, length = 100)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = TODO_DESCRIPTION, length = 200)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = TODO_DUE_DATE, nullable = false, length = 19)
	public String getDueDate() {
		return this.dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	@Column(name = TODO_CREATED_DATE, nullable = false, length = 19)
	public String getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = TODO_UPDATED_DATE, length = 19)
	public String getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Column(name = TODO_PRIORITY, length = 9)
	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((dueDate == null) ? 0 : dueDate.hashCode());
		result = prime * result
				+ ((priority == null) ? 0 : priority.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ToDoItem other = (ToDoItem) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (dueDate == null) {
			if (other.dueDate != null)
				return false;
		} else if (!dueDate.equals(other.dueDate))
			return false;
		if (priority == null) {
			if (other.priority != null)
				return false;
		} else if (!priority.equals(other.priority))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("ToDoItem[");
		sb.append(String.format("id=%d, ", id));
		sb.append(String.format("title=%d, ", id));
		sb.append(String.format("description=%s, ", description));
		sb.append(String.format("dueDate=%s, ", dueDate));
		sb.append(String.format("createdDate=%s, ", createdDate));
		sb.append(String.format("updatedDate=%s, ", createdDate));
		sb.append(String.format("priority=%s, ", priority)).append("]");
		return sb.toString();
	}

}
