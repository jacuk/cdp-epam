package com.epam.production.cdp.yatsenko.service;

import java.util.List;

import com.epam.production.cdp.yatsenko.dao.IToDoDAO;
import com.epam.production.cdp.yatsenko.dao.JpaToDoDAO;
import com.epam.production.cdp.yatsenko.entity.ToDoItem;

public class ToDoService {

	private final IToDoDAO todoDAO;

	public ToDoService() {
		todoDAO = new JpaToDoDAO();
	}

	public ToDoItem findById(long id) {
		return todoDAO.findById(id);
	}

	public List<ToDoItem> findAll() {
		return todoDAO.findAll();
	}

	public ToDoItem create(ToDoItem item) {
		return todoDAO.create(item);
	}

	public ToDoItem update(ToDoItem item) {
		return todoDAO.update(item);
	}

	public boolean remove(long id) {
		return todoDAO.remove(id);
	}

	public List<ToDoItem> findByPrioity(String priority) {
		return todoDAO.findByPriority(priority);
	}

	public List<ToDoItem> findEndingOn(String endingDate) {
		return todoDAO.findEndingOn(endingDate);
	}

}
