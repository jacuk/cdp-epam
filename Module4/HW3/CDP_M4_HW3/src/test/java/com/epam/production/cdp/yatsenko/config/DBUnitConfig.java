package com.epam.production.cdp.yatsenko.config;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.dbunit.DBTestCase;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.h2.tools.RunScript;
import org.junit.Before;

public class DBUnitConfig extends DBTestCase {
	private static final String SELECT_FROM_TODO_DB = "SELECT * FROM todo_db;";
	private static final String H2_DRIVER = "org.h2.Driver";
	private static final String TO_DO_DB_INIT = "ToDo_DB_create.sql";
	private static final String DB_CONFIG_PROPERTIES = "db.config.properties";
	private static final String H2_DB_DRIVER = "db.driver";
	private static final String H2_DB_URL = "db.url";
	private static final String H2_DB_USERNAME = "db.username";
	private static final String H2_DB_PASSWORD = "db.password";

	private final Properties prop;
	private final ClassLoader classLoader = this.getClass().getClassLoader();
	protected IDatabaseTester tester;
	protected IDataSet beforeData;

	@Override
	@Before
	public void setUp() throws Exception {
		tester = new JdbcDatabaseTester(prop.getProperty(H2_DB_DRIVER),
				prop.getProperty(H2_DB_URL), prop.getProperty(H2_DB_USERNAME),
				prop.getProperty(H2_DB_PASSWORD));
		initDb(prop);
	}

	public DBUnitConfig(String name) {
		super(name);
		prop = new Properties();
		try {
			prop.load(getFileInputStream(DB_CONFIG_PROPERTIES));
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		System.setProperty(
				PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS,
				prop.getProperty(H2_DB_DRIVER));
		System.setProperty(
				PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL,
				prop.getProperty("db.url"));
		System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME,
				prop.getProperty(H2_DB_USERNAME));
		System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD,
				prop.getProperty(H2_DB_PASSWORD));
		System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_SCHEMA, "");
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return beforeData;
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE_ALL;
	}

	protected InputStream getFileInputStream(String fileName) {
		return classLoader.getResourceAsStream(fileName);
	}

	/**
	 * @param prop
	 *            Initialize a database from a SQL script file.
	 * 
	 * @throws ClassNotFoundException
	 * @throws
	 */
	void initDb(Properties prop) throws SQLException, ClassNotFoundException {
		Class.forName(H2_DRIVER);
		InputStream in = getClass().getResourceAsStream(TO_DO_DB_INIT);
		if (in == null) {
			System.out.println(String.format(
					"Please add the file %s to the classpath, package %s ",
					TO_DO_DB_INIT, getClass().getPackage().getName()));
		} else {
			String url = prop.getProperty(H2_DB_URL);
			Connection conn = DriverManager.getConnection(url);
			RunScript.execute(conn, new InputStreamReader(in));
			Statement stat = conn.createStatement();
			ResultSet rs = stat.executeQuery(SELECT_FROM_TODO_DB);
			while (rs.next()) {
				System.out.println(rs.getString(1));
			}
			rs.close();
			stat.close();
			conn.close();
		}
	}

}