package com.epam.production.cdp.yatsenko;

import java.util.Arrays;
import java.util.List;

import org.dbunit.Assertion;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.epam.production.cdp.yatsenko.config.DBUnitConfig;
import com.epam.production.cdp.yatsenko.dao.IToDoDAO;
import com.epam.production.cdp.yatsenko.dao.JpaToDoDAO;
import com.epam.production.cdp.yatsenko.entity.ToDoItem;

public class ToDoTest extends DBUnitConfig {

	private static final String TODO_DB = "todo_db";
	private static final String ORIGINAL_DATA = "todo-data.xml";
	private static final String SAVE_DATA = "todo-data-save.xml";
	private static final String REMOVE_DATA = "todo-data-remove.xml";
	private static final String UPDATE_DATA = "todo-data-update.xml";

	public ToDoTest(String name) {
		super(name);
	}

	private final IToDoDAO todoDao = new JpaToDoDAO();
	private FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();

	@Override
	@Before
	public void setUp() throws Exception {
		super.setUp();
		beforeData = flatXmlDataSetBuilder
				.build(getFileInputStream(ORIGINAL_DATA));
		tester.setDataSet(beforeData);
		tester.onSetup();
	}

	@Test
	public void testFindAll() throws Exception {
		IDataSet expectedData = flatXmlDataSetBuilder
				.build(getFileInputStream(ORIGINAL_DATA));
		IDataSet actualData = tester.getConnection().createDataSet();
		String[] ignore = { "id" };
		Assertion.assertEqualsIgnoreCols(expectedData, actualData, TODO_DB,
				ignore);
	}

	@Test
	public void testFindById() {
		ToDoItem expectedToDoItem = new ToDoItem();
		expectedToDoItem.setId(1);
		expectedToDoItem.setPriority("NORMAL");
		expectedToDoItem.setDescription("Test1");
		expectedToDoItem.setTitle("Test1");
		expectedToDoItem.setCreatedDate("07/07/2014");
		expectedToDoItem.setDueDate("28/08/2014");
		expectedToDoItem.setUpdatedDate("09/07/2014");
		ToDoItem actualToDoItem = todoDao.findById(1);
		Assert.assertEquals(expectedToDoItem, actualToDoItem);
	}

	@Test
	public void testFindByPriority() {
		ToDoItem expectedToDoItem = new ToDoItem();
		expectedToDoItem.setId(1);
		expectedToDoItem.setDescription("Test1");
		expectedToDoItem.setTitle("Test1");
		expectedToDoItem.setCreatedDate("07/07/2014");
		expectedToDoItem.setDueDate("28/08/2014");
		expectedToDoItem.setUpdatedDate("09/07/2014");
		expectedToDoItem.setPriority("NORMAL");
		List<ToDoItem> expectedToDoItems = Arrays
				.asList(new ToDoItem[] { expectedToDoItem });
		List<ToDoItem> actualToDoItems = todoDao.findByPriority("NORMAL");
		Assert.assertEquals(expectedToDoItems, actualToDoItems);
	}

	@Test
	public void testFindEndingOn() {
		ToDoItem expectedToDoItem = new ToDoItem();
		expectedToDoItem.setId(1);
		expectedToDoItem.setDescription("Test1");
		expectedToDoItem.setTitle("Test1");
		expectedToDoItem.setCreatedDate("07/07/2014");
		expectedToDoItem.setDueDate("28/08/2014");
		expectedToDoItem.setUpdatedDate("09/07/2014");
		expectedToDoItem.setPriority("NORMAL");
		List<ToDoItem> expectedToDoItems = Arrays
				.asList(new ToDoItem[] { expectedToDoItem });
		List<ToDoItem> actualToDoItems = todoDao.findEndingOn("28/08/2014");
		Assert.assertEquals(expectedToDoItems, actualToDoItems);
	}

	@Test
	public void testSave() throws Exception {
		ToDoItem item = new ToDoItem();
		item.setTitle("Test3");
		item.setDescription("Test3");
		item.setCreatedDate("08/07/2014");
		item.setDueDate("30/08/2014");
		item.setPriority("LOW");
		item.setUpdatedDate("09/07/2014");
		todoDao.create(item);

		IDataSet expectedData = flatXmlDataSetBuilder
				.build(getFileInputStream(SAVE_DATA));

		IDataSet actualData = tester.getConnection().createDataSet();

		String[] ignore = { "id" };
		Assertion.assertEqualsIgnoreCols(expectedData, actualData, TODO_DB,
				ignore);
	}

	@Test
	public void testRemove() throws Exception {
		todoDao.remove(1);
		IDataSet expectedData = flatXmlDataSetBuilder
				.build(getFileInputStream(REMOVE_DATA));

		IDataSet actualData = tester.getConnection().createDataSet();

		String[] ignore = { "id" };
		Assertion.assertEqualsIgnoreCols(expectedData, actualData, TODO_DB,
				ignore);
	}

	@Test
	public void testUpdate() throws Exception {
		ToDoItem item = todoDao.findById(2);
		item.setDueDate("28/08/2015");
		todoDao.update(item);

		IDataSet expectedData = flatXmlDataSetBuilder
				.build(getFileInputStream(UPDATE_DATA));

		IDataSet actualData = tester.getConnection().createDataSet();

		String[] ignore = { "id" };
		Assertion.assertEqualsIgnoreCols(expectedData, actualData, TODO_DB,
				ignore);
	}

}
