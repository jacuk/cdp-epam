package com.epam.production.cdp.yatsenko.subTask3_4.timedLock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.epam.production.cdp.yatsenko.Account;

public class AccountTimedLock extends Account {

	public final Lock lock;

	public AccountTimedLock(final int initialBalance) {
		this.balance = initialBalance;
		lock = new ReentrantLock();
	}

}
