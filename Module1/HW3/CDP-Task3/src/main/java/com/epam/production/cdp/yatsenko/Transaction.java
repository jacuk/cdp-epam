package com.epam.production.cdp.yatsenko;


public class Transaction implements Runnable {

	private BankService bankService;
	
	public Transaction(BankService bank) {
		this.bankService = bank;
	}

	public void run() {
		try {
			int fromIndex = (int) (10 * Math.random());
			int toIndex = (int) (10 * Math.random());
			int amount = (int) (100 * Math.random());
			bankService.makeTransfer(fromIndex, toIndex, amount);
			Thread.sleep((long) (1000 * Math.random()));
		} catch (InterruptedException e) {
			System.out.println(e.getMessage());
		}
	}
}
