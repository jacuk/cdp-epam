package com.epam.production.cdp.yatsenko.subTask2.synch;

import com.epam.production.cdp.yatsenko.Account;
import com.epam.production.cdp.yatsenko.BankService;

public class BankServiceSynch extends BankService {

	public BankServiceSynch() {
		accounts = new AccountSynch[NUMBER_ACCOUNTS];
		for (int ind = 0; ind < accounts.length; ind++) {
			final Account acc = new AccountSynch(1000);
			accounts[ind] = acc;
		}
	}

	public synchronized boolean makeTransfer(int fromIndex, int toIndex, int amount)
			throws InterruptedException {
		Account accountFrom = accounts[fromIndex];
		Account accountTo = accounts[toIndex];
		synchronized (BankServiceSynch.class) {
			if (accountFrom.getBalance() < amount)
				return false;
			else {
				accountFrom.setBalance(accountFrom.getBalance() - amount);
				Thread.sleep((long) (100 * Math.random()));
				accountTo.setBalance(accountTo.getBalance() + amount);
				return true;
			}
		}
	}

}
