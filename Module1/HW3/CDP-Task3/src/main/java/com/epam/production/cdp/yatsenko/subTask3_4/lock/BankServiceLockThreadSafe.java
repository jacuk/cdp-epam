package com.epam.production.cdp.yatsenko.subTask3_4.lock;


public class BankServiceLockThreadSafe extends BankServiceLock {

	public BankServiceLockThreadSafe() {
		super();
	}
	
	@Override
	public double getTotal() {
		lock.lock();
		int summ = 0;
		for (AccountLock account : accounts) {
			summ += account.getBalance();
		}
		lock.unlock();
		return summ;
	}

}
