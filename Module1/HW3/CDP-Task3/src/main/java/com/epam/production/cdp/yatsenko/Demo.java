package com.epam.production.cdp.yatsenko;

import java.util.concurrent.TimeUnit;

import com.epam.production.cdp.yatsenko.subTask2.synch.BankServiceSynch;
import com.epam.production.cdp.yatsenko.subTask3_4.lock.BankServiceLock;
import com.epam.production.cdp.yatsenko.subTask3_4.timedLock.BankServiceTimedLock;

public class Demo {

	private static final int NUMBER_OF_TRANSACTION = 100;

	public static void main(String[] args) {
		printSubTask1();
		printSubTask2();
		printSubTask3();
		printSubTask4();
	}

	private static void printSubTask1() {
		System.out.println("--------------------------");
		System.out.println("|       Sub task 1       |");
		System.out.println("--------------------------");
		long startTime = System.nanoTime();
		BankService bankService = new BankServiceSynch();
		for (int i = 0; i < NUMBER_OF_TRANSACTION; i++) {
			new Thread((new Transaction(bankService))).start();
			if(i % 10 == 0) {
				printTotalAmount(bankService, false);
			}
		}
		printTotalAmount(bankService, true);
		printTotalTime(startTime);
	}
	
	private static void printSubTask2() {
		System.out.println("--------------------------");
		System.out.println("|       Sub task 2       |");
		System.out.println("--------------------------");
		long startTime = System.nanoTime();
		BankService bankService = new BankServiceSynch();
		Thread[] setThreads = startThreads(bankService);
		if(!waitEndOfThreads(setThreads)) {
			System.out.println("Error of thread finish");
			return;
		}
		printTotalAmount(bankService, true);
		printTotalTime(startTime);
	}

	private static void printSubTask3() {
		BankService bankService;
		System.out.println("--------------------------");
		System.out.println("|       Sub task 3       |");
		System.out.println("--------------------------");
		System.out.println("Without include getTotal");
		System.out.println("Lock");
		long startTime = System.nanoTime();
		bankService = new BankServiceLock();
		Thread[] setThreads = startThreads(bankService);
		if(!waitEndOfThreads(setThreads)) {
			System.out.println("Error of thread finish");
			return;
		}
		printTotalAmount(bankService, true);
		printTotalTime(startTime);

		startTime = System.nanoTime();
		System.out.println("TimedLock");
		BankService bankServiceTimedLock = new BankServiceTimedLock();
		setThreads = startThreads(bankService);
		if(!waitEndOfThreads(setThreads)) {
			System.out.println("Error of thread finish");
			return;
		}
		printTotalAmount(bankServiceTimedLock, true);
		printTotalTime(startTime);
	}

	private static void printSubTask4() {
		BankService bankService;
		System.out.println("--------------------------");
		System.out.println("|       Sub task 4       |");
		System.out.println("--------------------------");
		System.out.println("Lock");
		long startTime = System.nanoTime();
		bankService = new BankServiceLock();
		Thread[] setThreads = startThreads(bankService);
		if(!waitEndOfThreads(setThreads)) {
			System.out.println("Error of thread finish");
			return;
		}
		printTotalAmount(bankService, true);
		printTotalTime(startTime);
	}
	
	private static Thread[] startThreads(BankService bankService) {
		Thread[] threads = new Thread[NUMBER_OF_TRANSACTION];
		for (int i = 0; i < NUMBER_OF_TRANSACTION; i++) {
			threads[i] = new Thread((new Transaction(bankService)));
			threads[i].start();
			if(i % 10 == 0) {
				printTotalAmount(bankService, false);
			}
		}
		return threads;
	}

	private static boolean waitEndOfThreads(Thread[] setThreads) {
		try {
			for (int i = 0; i < setThreads.length; i++) {
				setThreads[i].join();
			}
		} catch (InterruptedException e) {
			return false;
		}
		return true;
	}
	
	private static void printTotalAmount(BankService bankServiceTimedLock, boolean finalAmount) {
		StringBuilder stringBuilder = new StringBuilder();
		if(finalAmount) {
			stringBuilder.append("Final ");
		}
		stringBuilder.append("Total amount: " + bankServiceTimedLock.getTotal());
		System.out.println(stringBuilder.toString());
	}
	
	private static void printTotalTime(long startTime) {
		System.out.println("Total time: "
				+ getTimeInSeconds(startTime, System.nanoTime())
				+ " milliseconds.\n");
	}

	private static long getTimeInSeconds(long startTime, long endTime) {
		return TimeUnit.NANOSECONDS.toMillis(endTime - startTime);
	}

}
