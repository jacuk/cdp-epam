package com.epam.production.cdp.yatsenko.subTask3_4.timedLock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BankServiceTim‫edLockThreadSafe extends BankServiceTimedLock {

	public BankServiceTim‫edLockThreadSafe() {
		transferLock = new ReentrantLock();
		for (int ind = 0; ind < accounts.length; ind++) {
			final AccountTimedLock acc = new AccountTimedLock(1000);
			accounts[ind] = acc;
			final Lock l = new ReentrantLock();
			locks[ind] = l;
		}
	}

	@Override
	public double getTotal() {
		int summ = 0;
		try {
			if (transferLock.tryLock(500, TimeUnit.SECONDS)) {
				for (int i = 0; i < locks.length; i++) {
					summ += accounts[i].getBalance();
				}
			}
		} catch (InterruptedException e) {
		} finally {
			transferLock.unlock();
		}
		return summ;
	}

}
