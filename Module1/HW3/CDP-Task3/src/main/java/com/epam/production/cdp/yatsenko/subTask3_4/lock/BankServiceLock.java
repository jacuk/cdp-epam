package com.epam.production.cdp.yatsenko.subTask3_4.lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.epam.production.cdp.yatsenko.BankService;


public class BankServiceLock extends BankService {

	protected final AccountLock[] accounts = new AccountLock[NUMBER_ACCOUNTS];
	protected Lock lock;

	public BankServiceLock() {
		lock = new ReentrantLock();
		for (int ind = 0; ind < accounts.length; ind++) {
			final AccountLock acc = new AccountLock(1000, ind);
			accounts[ind] = acc;
		}
	}

	public boolean makeTransfer(int fromIndex, int toIndex, int amount)
			throws InterruptedException {
		final AccountLock accountFrom = accounts[fromIndex];
		final AccountLock accountTo = accounts[toIndex];
		lockAccout(fromIndex, toIndex, accountFrom.lock, accountTo.lock);
		if (accountFrom.getBalance() < amount) {
			unlockAccout(fromIndex, toIndex, accountFrom.lock, accountTo.lock);
			return false;
		} else {
			accountFrom.setBalance(accountFrom.getBalance() - amount);
			Thread.sleep((long) (10 * Math.random()));
			accountTo.setBalance(accountTo.getBalance() + amount);
			unlockAccout(fromIndex, toIndex, accountFrom.lock, accountTo.lock);
			return true;
		}
	}

	private boolean lockAccout(int fromIndex, int toIndex, Lock fromLock,
			Lock toLock) throws InterruptedException {
		boolean lockResult = false;
		if (accounts[fromIndex].getId() < accounts[toIndex].getId()) {
			fromLock.lock();
			toLock.lock();
		} else if(accounts[fromIndex].getId() > accounts[toIndex].getId()) {
			toLock.lock();
			fromLock.lock();
		}
		return lockResult;
	}
	
	private void unlockAccout(int fromIndex, int toIndex, Lock fromLock,
			Lock toLock) throws InterruptedException {
		if (accounts[fromIndex].getId() < accounts[toIndex].getId()) {
			fromLock.unlock();
			toLock.unlock();
		} else if (accounts[fromIndex].getId() > accounts[toIndex].getId()) {
			toLock.unlock();
			fromLock.unlock();
		}
	}
	
	@Override
	public double getTotal() {
		int summ = 0;
		for (AccountLock account : accounts) {
			summ += account.getBalance();
		}
		return summ;
	}
}
