package com.epam.production.cdp.yatsenko.subTask3_4.timedLock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.epam.production.cdp.yatsenko.BankService;

public class BankServiceTimedLock extends BankService {

	protected AccountTimedLock[] accounts = new AccountTimedLock[10];
	protected Lock[] locks = new ReentrantLock[NUMBER_ACCOUNTS];
	protected Lock transferLock;

	public BankServiceTimedLock() {
		transferLock = new ReentrantLock();
		for (int ind = 0; ind < accounts.length; ind++) {
			final AccountTimedLock acc = new AccountTimedLock(1000);
			accounts[ind] = acc;
			final Lock l = new ReentrantLock();
			locks[ind] = l;
		}
	}

	public boolean makeTransfer(int fromIndex, int toIndex, int amount)
			throws InterruptedException {
		Lock fromLock = locks[fromIndex];
		Lock toLock = locks[toIndex];
		if (lockAccout(fromIndex, toIndex, fromLock, toLock)) {
			AccountTimedLock accountFrom = accounts[fromIndex];
			AccountTimedLock accountTo = accounts[toIndex];
			if (accountFrom.getBalance() < amount) {
				unlockAccout(fromIndex, toIndex, fromLock, toLock);
				transferLock.unlock();
				return false;
			} else {
				accountFrom.setBalance(accountFrom.getBalance() - amount);
				Thread.sleep((long) (10 * Math.random()));
				accountTo.setBalance(accountTo.getBalance() + amount);
				unlockAccout(fromIndex, toIndex, fromLock, toLock);
				transferLock.unlock();
				return true;
			}
		}
		return false;
	}

	private boolean lockAccout(int fromIndex, int toIndex, Lock fromLock,
			Lock toLock) throws InterruptedException {
		boolean lockResult = false;
		lockResult = fromLock.tryLock(500, TimeUnit.MILLISECONDS)
				&& toLock.tryLock(500, TimeUnit.MILLISECONDS);
		return lockResult;
	}

	private void unlockAccout(int fromIndex, int toIndex, Lock fromLock,
			Lock toLock) throws InterruptedException {
		fromLock.unlock();
		toLock.unlock();
	}

	@Override
	public double getTotal() {
		int summ = 0;
		for (int i = 0; i < locks.length; i++) {
			summ += accounts[i].getBalance();
		}
		return summ;
	}
}
