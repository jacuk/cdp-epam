package com.epam.production.cdp.yatsenko;

import java.util.List;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset.Entry;
import com.google.common.collect.Multisets;

public class WorkWithText {

	private HashMultiset<String> wordsMap;
	private String rawText;
	
	/**
	 * Save text in class;
	 * @param rawText
	 */
	public void setRawText(String rawText) {
		this.rawText = rawText;
	}
	
	/**
	 * Method returns the number of unique words in the text. Default delimiter
	 * is white space
	 * 
	 * @return uniqueWords.
	 */
	public int getUniqueWords() {
		buildWordsMap(rawText);
		return wordsMap.elementSet().size();
	}

	/**
	 * Method calculate, how many times the word appears in the text. If word
	 * not found, or the error occurred, method returns 0; Default delimiter is
	 * white space
	 * 
	 * @param wordSearch
	 *            word for search
	 * @return the number of words in the text.
	 */
	public int getFrequencyWords(String wordSearch) {
		buildWordsMap(rawText);		
		return wordsMap.count(wordSearch.toLowerCase());
	}
	
	/**
	 * Return last n most popular word
	 * @param n - quantity of popular words
	 * @return list of words
	 */
	public List<Entry<String>> getMostPopularWords(int n) {
		return Multisets.copyHighestCountFirst(wordsMap).entrySet().asList().subList(0, n);
	}
	
	/**
	 * Build map with words from text and number of words in the text. 
	 * @param text
	 */
	private void buildWordsMap(String text) {
		if(wordsMap != null) {
			return;
		}
		wordsMap = HashMultiset.create();
		List<String> wordList = Splitter.on(CharMatcher.anyOf(" \t\n\r,:-.\\s()[]{}?!-_—*;'\"")).trimResults().omitEmptyStrings().splitToList(text);
		for(String word : wordList) {
			wordsMap.add(word);
		}
	}
	
}
