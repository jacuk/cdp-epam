package com.epam.production.cdp.yatsenko;

import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.production.cdp.yatsenko.util.LoadDataUtils;
import com.google.common.collect.Multiset;


public class Demo {

	private static final String BEANS_XML = "/beans.xml";
	
	private static final String CODE_PAGE = "utf-8";
	
	public static void main(String[] args) throws IOException {

		//Check input parameters
		if (args.length != 1) {
			showUsage();
			return;
		}
		String fileName = args[0];
		String rawText = null;
		try {
			rawText = LoadDataUtils.load(fileName);
		} catch (IOException e) {
			System.err.println("Error reading: " + e.getMessage());
			return;
		}
		
		//Get WorkWithText from bean 
		@SuppressWarnings("resource")
		ApplicationContext context = new ClassPathXmlApplicationContext(BEANS_XML);
		WorkWithText workWithText = (WorkWithText) context.getBean("workWithText");
		workWithText.setRawText(rawText);
		System.out.println("Unique words in the text: " + workWithText.getUniqueWords());
		printMostPopularWord(workWithText);
		printWordFrequency(workWithText, new Scanner(System.in));
	}

	private static void printWordFrequency(WorkWithText workWithText,
			Scanner scanner) throws UnsupportedEncodingException {
		String searchWord;
		System.out.println("------------------------------------------");
		System.out.println("| Please, enter \"exit\" for close program |");
		System.out.println("------------------------------------------");
		System.out.print("Enter word for search: ");
		while(scanner.hasNext()) {
			searchWord = scanner.next();
			if(searchWord.toLowerCase().equals("exit")) {
				break;
			}
			System.setOut(new PrintStream(System.out, true, CODE_PAGE));
			System.out.print("Word \"" + searchWord + " "
					+ "\" mentioned in the text "
					+ workWithText.getFrequencyWords(searchWord) + " times.\n");
			System.out.print("Enter word for search: ");
		}
	}

	private static void printMostPopularWord(WorkWithText workWithText) {
		List<Multiset.Entry<String>> popularWords = workWithText.getMostPopularWords(10);
		Iterator<Multiset.Entry<String>> itr = popularWords.iterator();
		System.out.println("10 most popular words");
		while(itr.hasNext()) {
			Multiset.Entry<String> currentWord = itr.next(); 
			System.out.println("  - \"" + currentWord.getElement() + "\": " + currentWord.getCount());
		}
	}

	private static void showUsage() {
		System.out
				.println("Usage: java CDP_Task1 path_to_file");
	}

}
