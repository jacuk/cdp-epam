package com.epam.production.cdp.yatsenko;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

public class WorkWithTextTest {

	private static final String rawTextCorrect = "one two two three four five five five";
	private static final String rawTextIncorrect = "";
	private static WorkWithText workWithTextCorrect;
	private static WorkWithText workWithTextIncorrect;
	
	@Before
	public void setUp() throws Exception {
		workWithTextCorrect = new WorkWithText();
		workWithTextCorrect.setRawText(rawTextCorrect);
		workWithTextIncorrect = new WorkWithText();
		workWithTextIncorrect.setRawText(rawTextIncorrect);
	}
	
	/*********************************
	 * TEST getUniqueWords method
	 ********************************/
	
	@Test
	public void testGetUniqueWordsCorrect() {
		Assert.assertEquals(workWithTextCorrect.getUniqueWords(), 5);
	}
	
	@Test
	public void testGetUniqueWordsIncorrect() {
		Assert.assertEquals(workWithTextIncorrect.getUniqueWords(), 0);
	}
	
	/*********************************
	 * TEST getFrequencyWords method
	 ********************************/
	@Test
	public void testGetFrequencyWordsCorrect() {
		Assert.assertEquals(workWithTextCorrect.getFrequencyWords("five"), 3);
	}
	
	@Test
	public void testGetFrequencyWordsIncorrect() {
		Assert.assertEquals(workWithTextIncorrect.getFrequencyWords("five"), 0);
	}
	
}
