package com.epam.production.cdp.yatsenko;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

public class Demo {

	private static final int NUMBER_OF_TRANSACTION = 100;

	public static void main(String[] args) {
		System.out.println("CountDownLatch");
		long startTime = System.nanoTime();
		CountDownLatch latch = new CountDownLatch(NUMBER_OF_TRANSACTION);
		BankService bankService = new BankServiceLock();
		for (int i = 0; i < NUMBER_OF_TRANSACTION; i++) {
			new ThreadLatch(bankService, latch).start();
			if(i % 10 == 0) {
				printTotalAmount(bankService, false);
			}
		}
		try {
			latch.await();
		} catch (InterruptedException e) {
		}
		printTotalAmount(bankService, true);
		printTotalTime(startTime);
		
		System.out.println("CyclicBarier");
		startTime = System.nanoTime();
		CyclicBarrier barier = new CyclicBarrier(NUMBER_OF_TRANSACTION);
		bankService = new BankServiceLock();
		for (int i = 0; i < NUMBER_OF_TRANSACTION; i++) {
			Thread t = new Thread(new ThreadCyclic(bankService, barier));
			t.start();
			if(i % 10 == 0) {
				printTotalAmount(bankService, false);
			}
		}
		printTotalAmount(bankService, true);
		printTotalTime(startTime);
		
	}

	private static void printTotalAmount(BankService bankServiceTimedLock, boolean finalAmount) {
		StringBuilder stringBuilder = new StringBuilder();
		if(finalAmount) {
			stringBuilder.append("Final ");
		}
		stringBuilder.append("Total amount: " + bankServiceTimedLock.getTotal());
		System.out.println(stringBuilder.toString());
	}
	
	private static void printTotalTime(long startTime) {
		System.out.println("Total time: "
				+ getTimeInSeconds(startTime, System.nanoTime())
				+ " milliseconds.\n");
	}

	private static long getTimeInSeconds(long startTime, long endTime) {
		return TimeUnit.NANOSECONDS.toMillis(endTime - startTime);
	}

}
