package com.epam.production.cdp.yatsenko;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class ThreadCyclic implements Runnable {

	private BankService bankService;
	private CyclicBarrier barier;
	
	public ThreadCyclic(BankService bankService, CyclicBarrier barier) {
		this.bankService = bankService;
		this.barier = barier;
	}
	
	public void run() {
		try {
			barier.await();
		} catch (InterruptedException e) {
			return;
		} catch (BrokenBarrierException e) {
			return;
		}
		new Transaction(bankService);
	}
	
}
