package com.epam.production.cdp.yatsenko;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.epam.production.cdp.yatsenko.Account;

public class AccountLock extends Account  {

	public final Lock lock;
	private long id;
	
	public AccountLock(final int initialBalance, long id) {
		this.balance = initialBalance;
		this.id = id;
		lock = new ReentrantLock();
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(final int balance) {
		this.balance = balance;
	}

	public long getId() {
		return id;
	}
	
}
