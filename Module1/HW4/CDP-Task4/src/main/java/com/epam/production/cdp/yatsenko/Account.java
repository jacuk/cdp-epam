package com.epam.production.cdp.yatsenko;

public abstract class Account {

	protected int balance;

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

}
