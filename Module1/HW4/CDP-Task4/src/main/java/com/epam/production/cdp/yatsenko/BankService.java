package com.epam.production.cdp.yatsenko;



public abstract class BankService {

	public static final int NUMBER_ACCOUNTS = 10;
	protected Account accounts[];

	public abstract boolean makeTransfer(int fromIndex, int toIndex, int amount)
			throws InterruptedException;

	public double getTotal() {
		int summ = 0;
		for (Account account : accounts) {
			summ += account.getBalance();
		}
		return summ;
	}

}
