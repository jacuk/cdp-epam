package com.epam.production.cdp.yatsenko;

import java.util.concurrent.CountDownLatch;

public class ThreadLatch extends Thread {

	private final CountDownLatch latch;
	private BankService bankService;
	
	public ThreadLatch(BankService bankService, CountDownLatch latch) {
		this.bankService = bankService;
		this.latch = latch;
	}
	
	@Override
	public synchronized void start() {
		latch.countDown();
		new Transaction(bankService);
	}

}
