package com.epam.production.cdp.yatsenko.CDP_Task1;

import java.util.HashMap;
import java.util.StringTokenizer;

public class WorkWithText {

	private HashMap<String, Long> wordsMap;
	private String rawText;
	/**
	 * Constructor save text in class;
	 * @param rawText
	 */
	public WorkWithText(String rawText) {
		this.rawText = rawText;
	}
	
	/**
	 * Method returns the number of unique words in the text. Default delimiter
	 * is white space
	 * 
	 * @return uniqueWords.
	 */
	public long getUniqueWords() {
		buildWordsMap(rawText);
		if(wordsMap == null) {
			return 0;
		}
		return wordsMap.keySet().size();
	}

	/**
	 * Method calculate, how many times the word appears in the text. If word
	 * not found, or the error occurred, method returns 0; Default delimiter is
	 * white space
	 * 
	 * @param wordSearch
	 *            word for search
	 * @return the number of words in the text.
	 */
	public long getFrequencyWords(String wordSearch) {
		buildWordsMap(rawText);
		Long frequencyWord = wordsMap.get(wordSearch);
		if (frequencyWord == null) {
			return 0;
		}
		return wordsMap.get(wordSearch);
	}
	
	/**
	 * Build map with words from text and number of words in the text. 
	 * @param text
	 */
	private void buildWordsMap(String text) {
		if(wordsMap != null) {
			return;
		}
		wordsMap = new HashMap<String, Long>();
		StringTokenizer stringTokenizer = new StringTokenizer(text, " \t\n\r,:-.\\s()[]{}?!-*;'\"");
		while(stringTokenizer.hasMoreElements()) {
			String word = stringTokenizer.nextToken().toLowerCase();
			Long quantity = wordsMap.get(word);
			if(quantity != null) {
				wordsMap.put(word, quantity + 1);
			} else {
				wordsMap.put(word, 1L);
			}
		}
	}
	
}
