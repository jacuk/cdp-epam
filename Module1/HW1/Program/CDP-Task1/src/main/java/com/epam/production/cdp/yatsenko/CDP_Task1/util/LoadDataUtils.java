package com.epam.production.cdp.yatsenko.CDP_Task1.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class LoadDataUtils {

	public static final String load(String s) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		StringBuilder text = null;
		text = new StringBuilder();
		BufferedReader bufReader  = new BufferedReader(new FileReader(s));
		String cur = null;
		while((cur=bufReader.readLine()) != null) {
			text.append(cur);
		}
		bufReader.close();
		return text.toString();
	}
	
}
