package com.epam.production.cdp.yatsenko.CDP_Task1;

import java.io.IOException;
import java.io.PrintStream;

import com.epam.production.cdp.yatsenko.CDP_Task1.util.LoadDataUtils;

public class Demo {

	public static void main(String[] args) throws IOException {
		int argsLength = args.length;
		if (argsLength < 1 || argsLength > 2) {
			showUsage();
			return;
		}
		String fileName = args[0];
		String searchWord = null;
		if (argsLength == 2) {
			searchWord = args[1];
		}
		String rawText = null;
		try {
			rawText = LoadDataUtils.load(fileName);
		} catch (IOException e) {
			System.err.println("Error reading: " + e.getMessage());
			return;
		}
		WorkWithText workWithText = new WorkWithText(rawText);
		if (searchWord != null) {
			System.setOut(new PrintStream(System.out, true, "cp866"));
			System.out.print("Word \"" + searchWord + " "
					+ "\" mentioned in the text "
					+ workWithText.getFrequencyWords(searchWord) + " times.\n");
		}
		System.out.println("Unique words in the text: "
				+ workWithText.getUniqueWords());
	}

	private static void showUsage() {
		System.out
				.println("Usage: java CDP_Task1 path_to_file [word for search]");
	}

}
